var animals = ['gos', 'colom', 'gat', 'mosca', 'caball', 'serp'];

animals.sort(function(animal1, animal2){
    function isDomestic(lAnimal){
        if(lAnimal == 'gos' || lAnimal == 'gat' || lAnimal == 'caball'){
            return true;
        }else{
            return false;
        }
    }

    if(isDomestic(animal1) && !isDomestic(animal2)){
        return -1;
    }
    if(isDomestic(animal1) && isDomestic(animal2)){
        return -1;
    }
    if(!isDomestic(animal1) && isDomestic(animal2)){
        return 1;
    }
    if(!isDomestic(animal1) && !isDomestic(animal2)){
        return 0;
    }
})

console.log(animals);