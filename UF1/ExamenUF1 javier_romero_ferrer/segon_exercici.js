function comprovaNumeros(matricula){
    numeros_correctes = true;
    for(let index=0; index<4; index++){
        if((matricula.charCodeAt(index) < 48 || matricula.charCodeAt(index) > 57)){
            numeros_correctes = false;
        }
    }
    if(!numeros_correctes){
        return 'La part numèrica és incorrecta'
    }else{
        return ''
    }
}

function comprovaEspai(chart){
    if(chart.charCodeAt(0) == 32){
        return ''
    }else{
        return "L'espai separador és incorrecte"
    }
}

function comprovaLletres(matricula){
    lletres_correctes = true;
    for(let index=5; index<8; index++){
        if(matricula.charCodeAt(index) != 66 
        && matricula.charCodeAt(index) != 67
        && matricula.charCodeAt(index) != 68
        && matricula.charCodeAt(index) != 70
        && matricula.charCodeAt(index) != 71
        && matricula.charCodeAt(index) != 84){
            lletres_correctes = false;
        }
    }
    if(!lletres_correctes){
        return 'La part de les lletres és incorrecta'
    }else{
        return ''
    }
}

function testMatricula(matricula){
    console.log("\n--Test matrícula: " + matricula)
    missatges_error = []
    missatges_error.push(comprovaNumeros(matricula))
    missatges_error.push(comprovaEspai(matricula[4]))
    missatges_error.push(comprovaLletres(matricula))

    for(let index=0; index<missatges_error.length; index++){
        if(missatges_error[index] != ''){
            console.log(missatges_error[index])
        }
    }
}

testMatricula('1234 BCD');
testMatricula('A234VBN');
testMatricula('5678 MXZ');
testMatricula('J891 TFG');