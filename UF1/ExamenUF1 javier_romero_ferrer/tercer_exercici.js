llista_numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

//ordinaria
function arrEntre(num1, num2, llista){
    array_nou = [];
    for(let index=0; index<llista.length; index++){
        if(llista[index]>num1 && llista[index]<num2){
            array_nou.push(llista[index]);
        }
    }
    return array_nou;
}
console.log(arrEntre(3, 10, llista_numeros));

//fletxa
param1 = 2;
param2 = 11;
llista_valors_continguts = llista_numeros.filter((valor) => valor > param1 && valor < param2);
console.log(llista_valors_continguts);