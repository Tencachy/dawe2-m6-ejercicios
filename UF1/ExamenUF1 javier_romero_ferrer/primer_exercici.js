function sumaMatricial(matriu1, matriu2){
    matriu_result = [{}, {}, {}]
    if(matriu1.length == matriu2.length){
        for(let i=0; i<matriu1.length; i++){
            for(let j=0; j<matriu2.length; j++){
                if(matriu1[i].length == matriu2[j].length){
                    for(let fila=0; fila<matriu1.length; fila++){
                        for(let col=0; col<matriu1[fila].length; col++){
                            matriu_result[fila][col] = matriu1[fila][col] + matriu2[fila][col]
                        }
                    }
                    return matriu_result
                }else{
                    console.log("Les matrius no són iguals")
                }
            }
        }
    }else{
        console.log("Les matrius no són iguals")
    }
}

function restaMatricial(matriu1, matriu2){
    matriu_result = [{}, {}, {}]
    if(matriu1.length == matriu2.length){
        for(let i=0; i<matriu1.length; i++){
            for(let j=0; j<matriu2.length; j++){
                if(matriu1[i].length == matriu2[j].length){
                    for(let fila=0; fila<matriu1.length; fila++){
                        for(let col=0; col<matriu1[fila].length; col++){
                            matriu_result[fila][col] = matriu1[fila][col] - matriu2[fila][col]
                        }
                    }
                    return matriu_result
                }else{
                    console.log("Les matrius no són iguals")
                }
            }
        }
    }else{
        console.log("Les matrius no són iguals")
    }
}

mat1 = [[2, 0, 1], 
        [3, 0, 0], 
        [5, 1, 1]];

mat2 = [[1, 0, 1], 
        [1, 2, 1], 
        [1, 1, 0]];

console.log(sumaMatricial(mat1, mat2))
console.log('')
console.log(restaMatricial(mat1, mat2))