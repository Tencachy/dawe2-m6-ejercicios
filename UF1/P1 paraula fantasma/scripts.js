var lista = ["sapiens", "austrolopithecus", "prehistoria", "erectus", "fantasma"];
var num_random = Math.floor(Math.random() * lista.length);
var palabra = lista[num_random];
var num_fantasmas = Math.round(palabra.length / 3);
var posiciones_fantasmas = [];
var posicion_repetida = false;
console.log("Número de fantasmas", num_fantasmas);
        
seleccionar_posicion_fantasmas();
modificar_palabra();
document.getElementById("palabra").innerHTML = palabra;

function comprobacion() {
    if(lista[num_random] == document.getElementById("respuesta").value){
        alert("¡Correcto!")
    }else {
        alert("Error, vuelve a jugar")
    }
}

function seleccionar_posicion_fantasmas(){
    posiciones_fantasmas.push(Math.floor(Math.random()*palabra.length));
    console.log("Primer número random", posiciones_fantasmas);
    do {
        guardar_posicion_array();
    }while(posiciones_fantasmas.length<num_fantasmas)
    console.log("Las posiciones son todas diferentes")
    console.log(posiciones_fantasmas);
}
        
function guardar_posicion_array(){
    posicion_repetida = false;
    var pos_random = Math.floor(Math.random()*palabra.length);
    for(i=0; i<posiciones_fantasmas.length; i++){
        if (pos_random == posiciones_fantasmas[i]){
            posicion_repetida = true;
            console.log("Random repetido ", pos_random, posiciones_fantasmas)
        }
    }
    if(!posicion_repetida){
        posiciones_fantasmas.push(pos_random);
        console.log("Número añadido", posiciones_fantasmas);
    }
}

function modificar_palabra(){
    palabra = palabra.split('');
    for(i=0; i<posiciones_fantasmas.length; i++){
        palabra[posiciones_fantasmas[i]] = "!";
    }
    palabra = palabra.join('');
    sustituir_fantasmas();
}

function sustituir_fantasmas() {
    for(i=0; i<num_fantasmas; i++){
        palabra = palabra.replace("!", '<img src="fantasma.svg">');
    }
}

        