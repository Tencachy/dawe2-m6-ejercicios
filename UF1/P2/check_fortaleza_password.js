function testLlargada(password, array_error){
    if (!(password.length > 8 && password.length < 20)){
        array_error.push("La contrseña debe tener entre 8 y 20 carácteres.");
    }
}

function testMinuscula(password, array_error){
    var contador_minuscules = 0;
    for(index = 0; index < password.length; index++){
        if(password.charCodeAt(index) >= 97 && password.charCodeAt(index) <= 122){
            contador_minuscules++;
        }
    }
    if(!(contador_minuscules >= 3)){
        array_error.push("La contrseña debe contener 3 letras minúsculas.")
    }
}

function testMajuscula(password, array_error){
    let has_majus = false;
    for(index = 0; index < password.length; index++){
        if(password.charCodeAt(index) >= 65 && password.charCodeAt(index) <= 90){
            has_majus = true;
        }
    }
    if(!has_majus) array_error.push("La contraseña debe contener una letra mayúscula.")
}

function testCaractersRepetits(password, array_error){
    var contador_chars_repetits = 1;
    for(index = 0; index < password.length; index++){
        if(password[index] == password[index-1]){
            contador_chars_repetits++;
            if(contador_chars_repetits == 3){
                array_error.push("La contrseña no puede tener el mismo carácter repetido de forma seguida 3 veces.")
            }
        }else{
            contador_chars_repetits = 1;
        }
    }
}

function testDigits(password, array_error){
    var contador_digits = 0;
    for(index = 0; index < password.length; index++){
        if(password.charCodeAt(index) >= 48 && password.charCodeAt(index) <= 57){
            contador_digits++;
        }
    }
    if(!(contador_digits >= 2)){
        array_error.push("La contraseña debe tener mínimo 2 dígitos.")
    }
}

function testContrasenya(pass_usuari){
    var error = [];
    
    testLlargada(pass_usuari, error) 
    testMinuscula(pass_usuari, error) 
    testMajuscula(pass_usuari, error) 
    testCaractersRepetits(pass_usuari, error) 
    testDigits(pass_usuari, error)

    for(index in error){
        console.log(error[index]);
    }

    if(error.length > 0){
        return false;
    }else{
        console.log("¡Contraseña segura!")
        return true;
    }
}

let array_passwords = ['Ghhh', 'javier3212', 'QW3rtyuiopasdfghjklñzxcvbn', 'Javier1234']
for(index in array_passwords){
    console.log("Test - password '" + array_passwords[index] + "'")
    testContrasenya(array_passwords[index]);
    console.log('')    
}

