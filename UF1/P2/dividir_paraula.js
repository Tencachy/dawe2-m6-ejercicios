array_vocals_comparar = ['a', 'e', 'i', 'o', 'u'];

function separarVocalsDeConsonants(text) {
    var array_paraula = text.split('');
    var vocals = [];
    var consonants = [];
    for(index = 0; index < array_paraula.length; index++){
        if(esVocal(array_paraula[index])){
            vocals.push(array_paraula[index]);
        }else{
            consonants.push(array_paraula[index]);
        }
    }
    vocals = vocals.join('');
    consonants = consonants.join('');
    var paraula = vocals + consonants;
    
    return paraula;
}

function esVocal(caracter) {
    return array_vocals_comparar.indexOf(caracter) >= 0;
}

console.log('Palabra: Trigonometria ----- ', separarVocalsDeConsonants("Trigonometria"));
console.log('Palabra: cefal0podo ----- ', separarVocalsDeConsonants("cefal0podo"));
console.log('Palabra: otorrinolaringologo ----- ', separarVocalsDeConsonants("otorrinolaringologo"));