var coches = [
    ["seat","Codoba",1997,10000],
    ["Kia","Sorento",1994,1000],
    ["seat","Todelo",2000,10000],
    ["Fiat","Bravo",2010,10200],
    ["Fiat","500",2010,10000],
    ["Mercedes","Calse B",2001,39000],  
    ["seat","Ibiza",1993,10100],
    ["Alfa Romeo","Julieta",2002,10000],
    ["Alfa Romeo","159",2002,10400],
    ["Mercedes","Calse C",2001,1000],  
    ["Alfa Romeo","147",1990,10500],
    ["Fiat","Punto",1990,999],
    ["Citroen","Saxo",1980,10300],
    ["Renault","Superc 5",1980,12000],
    ["BWM","M3",2020,1000],
    ["Kia","Picanto",1990,1000],
    ["Alfa Romeo","spider",1970,14500],
    ["Mercedes","Calse A",1994,60100],  
    ["Mercedes","Calse D",2011,21221]  
];


//1
console.log('1.- ')
//Funció ordinaria
function mostrarCochesSinMarca(coches){
    var array_coches = [];
    for(var index=0; index<coches.length; index++){
        if(!((coches[index][0] == 'Alfa Romeo' || coches[index][0] == 'Kia') || coches[index][2] >= 2001)){
            //console.log('Error: ' + coches[index][0] + coches[index][2]);
            array_coches.push(coches[index]);
        }
    }
    return array_coches;
}
console.table(mostrarCochesSinMarca(coches));

//Funció anònima
var cochesSinMarcas = coches.filter(function(coche){return coche[2] < 2001 && coche[0] != 'Alfa Romeo' && coche[0] != 'Kia'})
console.table(cochesSinMarcas);

//Funció fletxa
var cochesSinMarcas = coches.filter((coche) => coche[2] < 2001 && coche[0] != 'Alfa Romeo' && coche[0] != 'Kia');
console.table(cochesSinMarcas);


//2
console.log('\n2.- ')
//Funció ordinaria
function ordenarPorPrecio(coches){
    var array_coches_aux = duplicarArray(coches);
    var aux;
    for(var i=0; i<array_coches_aux.length; i++){
        for(var j=0; j<array_coches_aux.length-i-1; j++){
            if(array_coches_aux[j][3] > array_coches_aux[j+1][3]){
                aux = array_coches_aux[j+1]
                array_coches_aux[j+1] = array_coches_aux[j]
                array_coches_aux[j] = aux
            }
        }
    }
    return array_coches_aux;
}
console.table(ordenarPorPrecio(coches));

//Funció anònima
var coches_ordenados_anonima = duplicarArray(coches);
var cochesOrdenados = coches_ordenados_anonima.sort(function(cocheA, cocheB){return cocheA[3] - cocheB[3]})
console.table(cochesOrdenados)

//Funció fletxa
var coches_ordenados_flecha = duplicarArray(coches);
var cochesOrdenados = coches_ordenados_flecha.sort((cocheA, cocheB) => cocheA[3] - cocheB[3]);
console.table(cochesOrdenados)


//3
console.log('\n3.- ')
//Funció ordinaria
function rebajarPrecio(coches){
    var array_coches_rebajados = duplicarArray(coches)
    for(var i=0; i<array_coches_rebajados.length; i++){
        array_coches_rebajados[i][3] = array_coches_rebajados[i][3] * ((100-20)/100);
    }
    return array_coches_rebajados;
}
console.table(rebajarPrecio(coches));


//Funció anònima
var coches_rebajados_anonima = duplicarArray(coches)
var cochesRebajados = coches_rebajados_anonima.map(function(coche){
    coche[3] = coche[3] * ((100-20)/100);
    return coche;
})
console.table(cochesRebajados);


//Funció fletxa
var coches_rebajados_flecha = duplicarArray(coches)
var cochesRebajados = coches_rebajados_flecha.map((coche) => {
    coche[3] = coche[3] * ((100-20)/100);
    return coche
})
console.table(cochesRebajados);

/*Función que duplica un array sin referencias para no afectar a los siguientes ejercicios*/
function duplicarArray(coches){
    var new_array = [{},{}];
    for(index in coches){
        new_array[index] = coches[index].slice(0);
    }
    return new_array;
}
