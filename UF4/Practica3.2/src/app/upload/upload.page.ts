import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.page.html',
  styleUrls: ['./upload.page.scss'],
})
export class UploadPage implements OnInit {

  marca: string
  modelo: string
  year: string
  precio: string

  constructor(private router: Router) { }

  ngOnInit() {
  }

  guardarMoto() {
    const form = new FormData();
    let image = (<HTMLInputElement>document.getElementsByName("foto")[0]).files[0];
    form.append("foto", image);
    form.append("marca", this.marca);
    form.append("modelo", this.modelo);
    form.append("year", this.year);
    form.append("precio", this.precio+"€");

    fetch("http://javier-romero-7e3.alwaysdata.net/api/moto/foto", {
      "method": "POST",
      "body": form
    })
      .then(response => {
        this.router.navigateByUrl('/home');
      })
      .catch(err => {
        console.error(err);
      });
  }

}
