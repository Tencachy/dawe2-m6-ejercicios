import { Component } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  input: any;

  constructor(public toastController: ToastController) {}

  conversor() {
    this.input = document.getElementById('inputCantidad');
    let euro  = this.input.value;

    let dolar = euro * 1.08;

    let resultat = ''
    if (euro != '') {
      resultat = euro + '€ equivale a $' + dolar;
    } else {
      resultat = 'Introduce una cantidad'
    }
    this.presentToast(resultat)
  }

  async presentToast(resultat) {
    const toast = await this.toastController.create({
      message: resultat,
      duration: 2000,
    });
    toast.present();
  }

}
