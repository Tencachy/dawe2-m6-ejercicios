const express = require("express");
const cors = require('cors');
const bodyParser = require('body-parser');
const multer = require('multer')
const storage = multer.diskStorage({
    destination: 'img/',
    filename: (req, file, cb) => {
        cb(null, file.originalname)
    }
})
const upload = multer({storage})

const app = express();
const baseUrl = '/api';
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/api', express.static(__dirname + '/img'))

var motos = []

var mysql = require("mysql");

var connection = mysql.createConnection({
    host: "mysql-javier-romero-7e3.alwaysdata.net",
    user: "245101",
    password: "juanelectronics123",
    database: "javier-romero-7e3_motos_api",
});

connection.connect(function (err) {

    const getMotos = (request, response) => {
        motos = []
        if (err) throw err;
        if (request.query.marca) {
            let marca = request.query.marca;
            connection.query("SELECT * FROM motos WHERE marca = ?", [marca], function (err, result, fields) {
                if (err) throw err;
                result.forEach((moto) => {
                    motos.push(moto)
                });
                response.send(JSON.stringify(motos))
            });
        } else {
            connection.query("SELECT * FROM motos", function (err, result, fields) {
                if (err) throw err;
                result.forEach((moto) => {
                    motos.push(moto)
                });
                response.send(JSON.stringify(motos))
            });
        }
    }
    app.get(baseUrl + '/motos', getMotos)

    const addMoto = (request, response) => {
        const { marca, modelo, year, foto, precio } = request.body

        connection.query("INSERT INTO motos (marca, modelo, year, foto, precio) VALUES (?, ?, ?, ?, ?)", [marca, modelo, year, foto, precio], function (err, result, fields) {
            console.log(result)
        });
        response.send("Moto " + JSON.stringify(request.body))
    }
    app.post(baseUrl + '/moto', addMoto)

    const addMotoWithImage = (request, response) => {
        const { marca, modelo, year, precio } = request.body
        const foto = 'http://javier-romero-7e3.alwaysdata.net/api/' + request.file.originalname

        connection.query("INSERT INTO motos (marca, modelo, year, foto, precio) VALUES (?, ?, ?, ?, ?)", [marca, modelo, year, foto, precio], function (err, result, fields) {
            console.log(result)
        });
        response.send("Moto " + JSON.stringify(request.body))
    }
    app.post(baseUrl + '/moto/foto', upload.single('foto'), addMotoWithImage)

    const deleteMoto = (request, response) => {
        let id = request.params.id

        connection.query("DELETE FROM motos WHERE id = ?", [id], function (err, result, fields) {
            console.log(result)
        });
        response.send("Moto " + id + " eliminada")
    }
    app.delete(baseUrl + '/moto/:id', deleteMoto)
});

//Inicialitzem el servei
const PORT = process.env.PORT || 3000; // Port
const IP = process.env.IP || null; // IP

app.listen(PORT, IP, () => {
    console.log("El servidor está inicialitzat en el puerto " + PORT);
});
