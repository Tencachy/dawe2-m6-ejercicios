import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  moto: any;

  constructor(private route: ActivatedRoute, private router: Router, public alertController: AlertController) { 
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.moto = this.router.getCurrentNavigation().extras.state.moto;
      }
    })
  }

  ngOnInit() {
  }

  async askDelete(idMoto) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: '¡Cuidado!',
      message: '¿Estás seguro que quieres eliminar esta moto?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          id: 'cancel-button',
        }, {
          text: 'Confirmar',
          id: 'confirm-button',
          handler: () => {
            this.deleteMoto(idMoto)
          }
        }
      ]
    });

    await alert.present();
  }

  deleteMoto(idMoto) {
    const url = "http://motos.puigverd.org/moto/" + idMoto;
    fetch(url, {
      "method": "DELETE"
    })
    .then(response => {
        this.router.navigateByUrl('/home');
      }
    );
  }

}
