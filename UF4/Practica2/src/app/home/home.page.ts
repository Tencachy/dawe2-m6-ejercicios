import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  motos = [];

  constructor(private router: Router) {}

  ngOnInit() {
    this.getMotos()
  }

  ionViewWillEnter() {
    this.getMotos()
  }

  async getMotos() {
    const respuesta = await fetch("http://motos.puigverd.org/motos");
    this.motos = await  respuesta.json();
  }

  showDetails(idMoto) {
    this.motos.forEach(moto => {
      if (moto.id == idMoto) {
        let navigationExtras: NavigationExtras = {
          state: {
            moto: moto
          }
        }
        this.router.navigate(['detail'], navigationExtras)
      }
    });
  }

  async getFilteredMotos(filter) {
    const respuesta = await fetch("http://motos.puigverd.org/motos?marca="+filter);
    this.motos = await  respuesta.json();
  }
}
