class Disc{
    nom;
    interpret;
    anyPublicacio;
    tipusMusica;
    localitzacio;
    prestat;
    urlPortada;

    constructor(){
        this.nom = '';
        this.interpret = '';
        this.anyPublicacio = '';
        this.tipusMusica = '';
        this.localitzacio = 0;
        this.prestat = false;
    }

    getNom(){
        return this.nom;
    }
    setNom(lNom){
        this.nom = lNom;
    }

    getInterpret(){
        return this.interpret;
    }
    setInterpret(lInterpret){
        this.interpret = lInterpret;
    }

    getAnyPublicacio(){
        return this.anyPublicacio;
    }
    setAnyPublicacio(lAnyPublicacio){
        this.anyPublicacio = lAnyPublicacio;
    }

    getTipusMusica(){
        return this.tipusMusica;
    }
    setTipusMusica(lTipusMusica){
        this.tipusMusica = lTipusMusica;
    }

    getLocalitzacio(){
        return this.localitzacio;
    }
    setLocalitzacio(lLocalitzacio){
        this.localitzacio = lLocalitzacio;
    }

    getUrlPortada(){
        return this.urlPortada;
    }
    setUrlPortada(lUrlPortada){
        this.urlPortada = lUrlPortada;
    }

    getPrestat(){
        return this.prestat;
    }
    setPrestat(lPrestat){
        this.prestat = lPrestat;
    }

    addProperties(lNom, lInterpret, lAnyPublicacio, lTipusMusica, lLocalitzacio, lUrlPortada){
        this.nom = lNom;
        this.interpret = lInterpret;
        this.anyPublicacio = lAnyPublicacio;
        this.tipusMusica = lTipusMusica;
        this.localitzacio = lLocalitzacio;
        this.urlPortada = lUrlPortada;
    }

    getDiscInformation(){
        console.log('Nom: ' + this.nom)
        console.log('Interpret: ' + this.interpret.getNom())
        console.log('Any de publicació: ' + this.anyPublicacio)
        console.log('Tipus de música: ' + this.tipusMusica)
        console.log('Localització: ' + this.localitzacio)
        console.log('Prestat: ' + this.prestat)
        console.log('Url de la portada: ' + this.urlPortada + '\n')
    }
}

class Interpret{
    nom;
    anyNaixement;

    constructor(lNom, lAnyNaixement){
        this.nom = lNom;
        this.anyNaixement = lAnyNaixement;
    }

    getNom(){
        return this.nom;
    }
    setNom(lNom){
        this.nom = lNom;
    }

    getAnyNaixement(){
        return this.anyNaixement;
    }
    setNom(lAnyNaixement){
        this.anyNaixement = lAnyNaixement;
    }
}

class Discografia{
    discos = [];

    add(disc){
        this.discos.push(disc);
    }

    delete(nomDisc){
        for(let index=0; index<this.discos.length; index++){
            if(this.discos[index].getNom() == nomDisc){
                this.discos.splice(index, 1);
                console.log('Disco \'' + nomDisc + '\' eliminado')
            }
        }
    }

    ordenar(metodeOrdenacio){
        switch(metodeOrdenacio){
            case 'nom':
            case 'Nom':
                console.log('ordenar por nombre')
                this.discos.sort(function(disc1, disc2){
                    if(disc1.getNom() > disc2.getNom()) return 1;
                    else if(disc1.getNom() < disc2.getNom()) return -1;
                    return 0;
                });
                break;
            case 'nom del grup':
            case 'Nom del grup':
                this.discos.sort(function(disc1, disc2){
                    if(disc1.getInterpret().getNom() > disc2.getInterpret().getNom()) return 1;
                    else if(disc1.getInterpret().getNom() < disc2.getInterpret().getNom()) return -1;
                    return 0;
                });
                break;
            case 'any': 
            case 'Any':
                this.discos.sort(function(disc1, disc2){
                    if(disc1.getAnyPublicacio() > disc2.getAnyPublicacio()) return 1;
                    else if(disc1.getAnyPublicacio() < disc2.getAnyPublicacio()) return -1;
                    return 0;
                });
                break;
            case 'tipus de música': 
            case 'Tipus de música':
                this.discos.sort(function(disc1, disc2){
                    return disc1.getTipusMusica().charCodeAt(0) - disc2.getTipusMusica().charCodeAt(0);
                });
                break;
            case 'localització': 
            case 'localitzacio':
            case 'Localitzacio':
            case 'Localització':
                this.discos.sort(function(disc1, disc2){
                    return disc1.getLocalitzacio() - disc2.getLocalitzacio();
                });
                break;
        }
    }

    filtrar(atribut, filtre){
        var discos_aux;
        switch(atribut){
            case 'nom':
            case 'Nom':
                discos_aux = this.discos.filter((disc)=> disc.getNom() == filtre);
                break;
            case 'nom del grup':
            case 'Nom del grup':
                discos_aux = this.discos.filter((disc)=> disc.getInterpret().getNom() == filtre);
                break;
            case 'any': 
            case 'Any':
                discos_aux = this.discos.filter((disc)=> disc.getAnyPublicacio() == filtre);
                break;
            case 'tipus de música': 
            case 'Tipus de música':
                discos_aux = this.discos.filter((disc)=> disc.getTipusMusica() == filtre);
                break;
            case 'localització': 
            case 'localitzacio':
            case 'Localitzacio':
            case 'Localització':
                discos_aux = this.discos.filter((disc)=> disc.getLocalitzacio() == filtre);
                break;
        }
        discos_aux.forEach(disc =>{
            disc.getDiscInformation();
        })
    }

    llistar(){
        this.discos.forEach(disc => {
            console.log('Nom: ' + disc.getNom())
            console.log('Interpret: ' + disc.getInterpret().getNom())
            console.log('Any de publicació: ' + disc.getAnyPublicacio())
            console.log('Tipus de música: ' + disc.getTipusMusica())
            console.log('Localització: ' + disc.getLocalitzacio())
            console.log('Prestat: ' + disc.getPrestat())
            console.log('----------')
        });
    }
}

disc1 = new Disc();
interpret1 = new Interpret("Guns N' Roses", '1985');
disc1.addProperties('Appetite for Destruction', interpret1, '1987', 'rock', 6, 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/mejores-albumes-historia-guns-roses-appetite-destruction-1623775317.jpg?crop=1xw:1xh;center,top&resize=980:*');

disc2 = new Disc();
interpret2 = new Interpret("Pink Floyd", '1964');
disc2.addProperties('The Dark Side of the Moon', interpret2, '1973', 'rock', 1, 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/mejores-albumes-historia-pink-floyd-dark-side-moon-1623775315.jpg?crop=1xw:1xh;center,top&resize=980:*')

disc3 = new Disc();
interpret3 = new Interpret("Britney Spears", '1981');
disc3.addProperties('In the Zone', interpret3, '2003', 'pop', 3, 'https://images.coveralia.com/audio/b/Britney_Spears-In_The_Zone-Frontal.jpg?485')

disc4 = new Disc();
interpret4 = new Interpret("Daft Punk", '1993');
disc4.addProperties('Random Access Memorie', interpret4, '2013', 'pop', 4, 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/mejores-discos-pop-daft-punk-1626191762.jpg?crop=1xw:1xh;center,top&resize=980:*')

disc5 = new Disc();
interpret5 = new Interpret("Siniestro Total", '1981');
disc5.addProperties('¿Cuándo se come aquí?', interpret5, '1982', 'punk', 12, 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/punk-rock-siniestro-total-1606333851.jpg?crop=1xw:1xh;center,top&resize=980:*')

disc6 = new Disc();
interpret6 = new Interpret("Taylor Swift", '1989');
disc6.addProperties("Reputation", interpret6, '2017', 'pop', 14, 'https://www.lahiguera.net/musicalia/artistas/taylor_swift/disco/8600/taylor_swift_reputation-portada.jpg')

disc7 = new Disc();
disc7.addProperties("Red (Taylor's Version)", interpret6, '2021', 'pop', 14, 'https://upload.wikimedia.org/wikipedia/en/4/47/Taylor_Swift_-_Red_%28Taylor%27s_Version%29.png')

disc8 = new Disc();
interpret8 = new Interpret("Duki", '1996');
disc8.addProperties("Súper Sangre Joven", interpret8, '2019', 'indie', 21, 'https://upload.wikimedia.org/wikipedia/en/5/57/Duki_-_S%C3%BAper_sangre_joven.jpeg')

disc9 = new Disc();
interpret9 = new Interpret("Eminem", '1972');
disc9.addProperties("The Eminem Show", interpret9, '2002', 'indie', 22, 'https://m.media-amazon.com/images/I/71n0xmxpw7L._SX355_.jpg')

disc10 = new Disc();
interpret10 = new Interpret("Don Omar", '1978');
disc10.addProperties("iDon", interpret10, '2009', 'indie', 21, 'https://upload.wikimedia.org/wikipedia/en/thumb/b/b2/IDon2009.jpg/220px-IDon2009.jpg')

discografia1 = new Discografia();
discografia1.add(disc1);
discografia1.add(disc2);
discografia1.add(disc3);
discografia1.add(disc4);
discografia1.add(disc5);

console.log('---------- Listado discografia 5 discos ----------')
discografia1.llistar();

discografia1.add(disc6);
discografia1.add(disc7);
discografia1.add(disc8);
discografia1.add(disc9);
discografia1.add(disc10);

console.log('\n\n---------- Listado discografia 10 discos ----------')
discografia1.llistar();

console.log('\n\n---------- Listado discografia ordenado por NOMBRE ----------')
discografia1.ordenar('nom');
discografia1.llistar();

console.log('\n\n---------- Listado discografia ordenado por AÑO DE PUBLICACIÓN ----------')
discografia1.ordenar('any');
discografia1.llistar();

console.log('\n\n---------- Filtrar por música INDIE ----------');
discografia1.filtrar('tipus de música', 'indie');

console.log('\n\n---------- Filtrar por INTÉRPRETE/NOMBRE DEL GRUPO ----------');
discografia1.filtrar('nom del grup', 'Taylor Swift');

console.log('\n\n---------- Eliminar segundo disco ----------')
discografia1.delete(disc2.getNom());

console.log('\n\n---------- Listado discografia ordenado por TIPO DE MÚSICA ----------')
discografia1.ordenar('tipus de música');
discografia1.llistar();