class Immoble {
    adreça;
    superfície;
    referència_cadastral;
    coordenades_geogràfiques;
    nou;
    preu_base;
    preu_final;
    anys;

    constructor(pAdreça, pSuperficie, pRefCadastral, pCoordGeo, pNou, pPreuBase, pAnys) {
        this.adreça = pAdreça;
        this.superfície = pSuperficie;
        this.referència_cadastral = pRefCadastral;
        this.coordenades_geogràfiques = pCoordGeo;
        this.nou = pNou;
        this.preu_base = pPreuBase;
        this.anys = pAnys;
    }

    calcularPreu() {
        if (this.anys > 10 && this.anys <= 20) {
            var descompte = this.anys - 10;
            this.preu_final = this.preu_base - ((descompte / 100) * this.preu_base);
        } else if (this.anys > 20) {
            this.preu_final = this.preu_base - ((10 / 100) * this.preu_base);
        } else {
            this.preu_final = this.preu_base;
        }
    }

    mostrarDetalls() {
        console.log('Adreça: ' + this.adreça)
        console.log('Metres quadrats: ' + this.superfície + 'm^2')
        console.log('Referència cadastral: ' + this.referència_cadastral)
        console.log('Coordenades geogràfiques: ' + this.coordenades_geogràfiques)
        this.nou ? console.log('Pis nou') : console.log('Pis de segona mà')
        console.log('Preu base: ' + this.preu_base + '€')
        console.log('Antiguetat: ' + this.anys + ' anys')
    }

    getPreuFinal() {
        return this.preu_final;
    }
}

class Pis extends Immoble {
    pis;
    ascensor;

    constructor(pAdreça, pSuperficie, pRefCadastral, pCoordGeo, pNou, pPreuBase, pAnys, pPis, pAscensor) {
        super(pAdreça, pSuperficie, pRefCadastral, pCoordGeo, pNou, pPreuBase, pAnys);
        this.pis = pPis;
        this.ascensor = pAscensor;
    }

    calcularPreu() {
        super.calcularPreu();
        if (this.pis >= 3 && this.ascensor) {
            this.preu_final = this.preu_final + ((3 / 100) * this.preu_base);
        }
    }

    mostrarDetalls() {
        super.mostrarDetalls();
        console.log('Pis/planta: ' + this.pis)
        this.ascensor ? console.log('Amb ascensor') : console.log('Sense ascensor')
    }
}

class Local extends Immoble {
    num_finestres;
    persiana_metàlica;
    static num_locals_comercials = 0;
    static num_locals_industrials = 0;
    static num_locals_restauracio = 0;

    constructor(pAdreça, pSuperficie, pRefCadastral, pCoordGeo, pNou, pPreuBase, pAnys, pNumFinestres, pPersiana) {
        super(pAdreça, pSuperficie, pRefCadastral, pCoordGeo, pNou, pPreuBase, pAnys);
        this.num_finestres = pNumFinestres;
        this.persiana_metàlica = pPersiana;
    }

    calcularPreu() {
        super.calcularPreu();
        if (this.superfície > 50) {
            this.preu_final = this.preu_final + ((1 / 100) * this.preu_base);
        }
        if (this.num_finestres <= 1) {
            this.preu_final = this.preu_final - ((2 / 100) * this.preu_base);
        } else if (this.num_finestres > 4) {
            this.preu_final = this.preu_final + ((2 / 100) * this.preu_base);
        }
    }

    mostrarDetalls() {
        super.mostrarDetalls();
        console.log('Número de finestres: ' + this.num_finestres)
        this.persiana_metàlica ? console.log('Amb persiana metàl·lica') : console.log('Sense persiana metàl·lica')
    }
}

class LocalComercial extends Local {
    adaptat;

    constructor(pAdreça, pSuperficie, pRefCadastral, pCoordGeo, pNou, pPreuBase, pAnys, pNumFinestres, pPersiana, pAdaptat) {
        super(pAdreça, pSuperficie, pRefCadastral, pCoordGeo, pNou, pPreuBase, pAnys, pNumFinestres, pPersiana);
        this.adaptat = pAdaptat;
        Local.num_locals_comercials++;
    }

    mostrarDetalls() {
        super.mostrarDetalls()
        this.adaptat ? console.log('Adaptat per a persones impedides') : console.log('No està adaptat per a persones impedides')
    }
}

class LocalIndustrial extends Local {
    port_carga_descarga;
    sol_industrial;

    constructor(pAdreça, pSuperficie, pRefCadastral, pCoordGeo, pNou, pPreuBase, pAnys, pNumFinestres, pPersiana, pPortCarga, pSolIndustrial) {
        super(pAdreça, pSuperficie, pRefCadastral, pCoordGeo, pNou, pPreuBase, pAnys, pNumFinestres, pPersiana);
        this.port_carga_descarga = pPortCarga;
        this.sol_industrial = pSolIndustrial;
        Local.num_locals_industrials++;
    }

    calcularPreu() {
        super.calcularPreu();
        if (!this.sol_industrial) {
            this.preu_final = this.preu_final + ((25 / 100) * this.preu_final);
        }
    }

    mostrarDetalls() {
        super.mostrarDetalls()
        this.port_carga_descarga ? console.log('Amb port de carga i descarga') : console.log('Sense port de carga i descarga')
        this.sol_industrial ? console.log('En sòl industrial') : console.log('En sòl urbà')
    }
}

class LocalRestauració extends Local {
    campana_extractora;
    cafetera;
    mobiliari;

    constructor(pAdreça, pSuperficie, pRefCadastral, pCoordGeo, pNou, pPreuBase, pAnys, pNumFinestres, pPersiana, pCampana, pCafetera, pMobiliari) {
        super(pAdreça, pSuperficie, pRefCadastral, pCoordGeo, pNou, pPreuBase, pAnys, pNumFinestres, pPersiana);
        this.campana_extractora = pCampana;
        this.cafetera = pCafetera;
        this.mobiliari = pMobiliari;
        Local.num_locals_restauracio++;
    }

    mostrarDetalls() {
        super.mostrarDetalls()
        this.campana_extractora ? console.log('Amb campana extractora') : console.log('Sense campana extractora')
        this.cafetera ? console.log('Amb cafetera') : console.log('Sense cafetera')
        this.mobiliari ? console.log('Amb mobiliari') : console.log('Sense mobiliari')
    }
}

console.log('TESTS');
//IMMOBLE
console.log('\n-----Test immoble-----')
immoble1 = new Immoble('c/Antonio Machado', 80, '123456789', '419, 53', true, 100000, 16);
immoble1.mostrarDetalls();
immoble1.calcularPreu();
console.log('Preu final: ' + immoble1.getPreuFinal() + '€')

//PIS
console.log('\n-----Test pis-----')
pis1 = new Pis('c/Aiguablava', 125, '987654321', '50, 92', false, 100000, 6, 4, true);
pis1.mostrarDetalls();
pis1.calcularPreu();
console.log('Preu final: ' + pis1.getPreuFinal() + '€')

console.log('\n-----Test pis-----')
pis2 = new Pis('c/Federico', 94, '456287913', '415, -832', true, 140000, 21, 10, true);
pis2.mostrarDetalls();
pis2.calcularPreu();
console.log('Preu final: ' + pis2.getPreuFinal() + '€')

//LOCAL
console.log('\n-----Test local-----')
local1 = new Local('c/Tornillo', 180, '344687912', '45, 24', false, 80000, 7, 20, true);
local1.mostrarDetalls();
local1.calcularPreu();
console.log('Preu final: ' + local1.getPreuFinal() + '€')


//LOCAL COMERCIAL
console.log('\n-----Test local comercial-----')
localcomercial1 = new LocalComercial('c/Girasol', 42, '123987654', '47, 456', true, 54000, 12, 2, false, false);
localcomercial1.mostrarDetalls();
localcomercial1.calcularPreu();
console.log('Preu final: ' + localcomercial1.getPreuFinal() + '€')

console.log('\n-----Test local comercial-----')
localcomercial2 = new LocalComercial('c/Enciclopedia', 76, '678459123', '38, 75', false, 62000, 14, 3, true, true);
localcomercial2.mostrarDetalls();
localcomercial2.calcularPreu();
console.log('Preu final: ' + localcomercial2.getPreuFinal() + '€')

console.log('\n-----Test local comercial-----')
localcomercial3 = new LocalComercial('c/Willyrex', 460, '346798125', '428, 379', true, 210000, 2, 230, true, true);
localcomercial3.mostrarDetalls();
localcomercial3.calcularPreu();
console.log('Preu final: ' + localcomercial3.getPreuFinal() + '€')

//LOCALS INDUSTRIALS
console.log('\n-----Test local industrial-----')
localindustrial1 = new LocalIndustrial('c/Cefalópodo', 380, '466798135', '467, 198', true, 364000, 28, 400, true, true, false);
localindustrial1.mostrarDetalls();
localindustrial1.calcularPreu();
console.log('Preu final: ' + localindustrial1.getPreuFinal() + '€')

console.log('\n-----Test local industrial-----')
localindustrial2 = new LocalIndustrial('c/Illojuan', 380, '466798114', '478, 162', true, 364000, 26, 380, true, true, true);
localindustrial2.mostrarDetalls();
localindustrial2.calcularPreu();
console.log('Preu final: ' + localindustrial2.getPreuFinal() + '€')

//LOCALS RESTAURACIÓ
console.log('\n-----Test local restauració-----')
localrestauracio1 = new LocalRestauració('c/Pingüino', 230, '34675821', '0, 1', false, 311000, 12, 32, true, false, true, true);
localrestauracio1.mostrarDetalls();
localrestauracio1.calcularPreu();
console.log('Preu final: ' + localrestauracio1.getPreuFinal() + '€')

console.log('\n-----Test local restauració-----')
localrestauracio2 = new LocalRestauració('c/Mandarina', 60, '3461987582', '431, 794', true, 46000, 4, 1, true, true, false, false);
localrestauracio2.mostrarDetalls();
localrestauracio2.calcularPreu();
console.log('Preu final: ' + localrestauracio2.getPreuFinal() + '€')

console.log('\n-----Número de locals:')
console.log('Locals comercials: ' + Local.num_locals_comercials)
console.log('Locals industrials: ' + Local.num_locals_industrials)
console.log('Locals restauració: ' + Local.num_locals_restauracio)