var request = require("request");

//Comprova que en aquesta url tenim un array d'objectes
var options = {
    method: 'GET',
    url: 'https://opendata-ajuntament.barcelona.cat/data/api/action/datastore_search?resource_id=f59e276c-1a1e-4fa5-8c89-8a8a56e56b34'
};

request(options, function(error, response, body) {
    if (error) throw new Error(error);

    var elMeuArray = JSON.parse(body);

    arrayEstacions = [];
    elMeuArray.result.records.forEach(bikestation => {
        if (bikestation.bikes > 10) {
            arrayEstacions.push({ streetName: bikestation.streetName, latitude: bikestation.latitude, longitude: bikestation.longitude, bikes: bikestation.bikes });
        }
    });
    var estacionsOrdenades = arrayEstacions.sort((estacio1, estacio2) => estacio1.bikes - estacio2.bikes);
    console.table(estacionsOrdenades);
});