class Batut{
    ingredients;
    cost;
    preus = [['Freses', 1.50], ['Platan', 0.50], ['Mango', 2.50], ['Fruits del bosc', 1.00], ['Coco', 1.00], ['Poma', 1.75], ['Piña', 3.50]];

    constructor(myIngredients){
        this.ingredients = myIngredients;
    }

    getCost(){
        this.cost = 0;
        /*this.ingredients.forEach(ingredient => {
            switch(ingredient){
                case 'Freses':
                    this.cost += 1.50;
                    break;
                case 'Platan':
                    this.cost += 0.50;
                    break;
                case 'Mango':
                    this.cost += 2.50;
                    break;
                case 'Fruits del bosc':
                    this.cost += 1.00;
                    break;
                case 'Coco':
                    this.cost += 1.00;
                    break;
                case 'Poma':
                    this.cost += 1.75;
                    break;
                case 'Piña':
                    this.cost += 3.50;
                    break;
            }
        });*/
        this.ingredients.forEach(ingredient => {
            this.preus.forEach(preuIngredient => {
                if(ingredient == preuIngredient[0]){
                    this.cost += preuIngredient[1];
                }
            });
        });
        return 'Cost: ' + this.cost;
    }

    getPreu(){
        var preu = Math.round((this.cost*1.5) * 100) / 100;
        return 'Preu: ' + preu;
    }

    getDescripcio(){
        if(this.ingredients.length <= 1){
            return 'Batut de: ' + this.ingredients;
        }else{
            this.ingredients.sort((ingredient1, ingredient2) => ingredient1.charCodeAt(0) - ingredient2.charCodeAt(0));
            return 'Fusió de: '+ this.ingredients;
        }
    }

    getHtml(){
        var html = '<ul>';
        this.ingredients.forEach(ingredient => {
            html = html + '<li>' + ingredient + '</li>';
        });
        html = html + '</ul>';
        return html;
    }
}

console.log("\n----- Batut 1: -----")
batut1 = new Batut(['Mango', 'Fruits del bosc', 'Platan'])
console.log(batut1.getCost());
console.log(batut1.getPreu());
console.log(batut1.getDescripcio());
console.log(batut1.getHtml());

console.log("\n----- Batut 2: -----")
batut2 = new Batut(['Platan'])
console.log(batut2.getCost());
console.log(batut2.getPreu());
console.log(batut2.getDescripcio());
console.log(batut2.getHtml());

console.log("\n----- Batut 3: -----")
batut3 = new Batut(['Poma', 'Piña', 'Freses'])
console.log(batut3.getCost());
console.log(batut3.getPreu());
console.log(batut3.getDescripcio());
console.log(batut3.getHtml());