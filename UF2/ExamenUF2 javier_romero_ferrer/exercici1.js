class Cataleg{
    rellotges = [];
    total = 0;
    totalMecanics = 0;
    totalElectronics = 0;
    totalSmartwatchs = 0;

    addRellotge(rellotge){
        this.rellotges.push(rellotge);
        this.total++;
        rellotge.precisio ? this.totalMecanics++ : this.totalMecanics = this.totalMecanics;
        rellotge.pila ? this.totalElectronics++ : this.totalElectronics = this.totalElectronics;
        rellotge.sistema_operatiu ? this.totalSmartwatchs++ : this.totalSmartwatchs = this.totalSmartwatchs;
    }

    print(){
        console.log('\n--- Llista actual de rellotges ---')
        let index = 0;
        this.rellotges.forEach(rellotge => {
            index++;
            console.log(index + '.- ' + rellotge.print())
        });
        console.log('Rellotges mecanics totals: ' + this.totalMecanics);
        console.log('Rellotges electronics totals: ' + this.totalElectronics);
        console.log('Rellotges smartwatch totals: ' + this.totalSmartwatchs);
        return 'Rellotges totals: ' + this.total;
    }
}

class Rellotge{
    marca;
    model;
    autonomia;

    constructor(lMarca, lModel, lAutonomia){
        this.marca = lMarca;
        this.model = lModel;
        this.autonomia = lAutonomia;
    }

    print(){
        let especificacions = 'Marca: ' + this.marca + 
        ' | Model: ' + this.model +
        ' | Autonomia: ' + this.autonomia + 'h';
        return especificacions;
    }
}

class Mecanic extends Rellotge{
    precisio;

    constructor(lMarca, lModel, lAutonomia, lPrecisio){
        super(lMarca, lModel, lAutonomia);
        this.precisio = lPrecisio;
    }
}

class Electronic extends Rellotge{
    tipus;
    pila;

    constructor(lMarca, lModel, lAutonomia, lTipus, lPila){
        super(lMarca, lModel, lAutonomia);
        this.tipus = lTipus;
        this.pila = lPila;
    }
}

class Smartwatch extends Rellotge{
    tipus;
    sistema_operatiu;

    constructor(lMarca, lModel, lAutonomia, lTipus, lSistemaOperatiu){
        super(lMarca, lModel, lAutonomia);
        this.tipus = lTipus;
        this.sistema_operatiu = lSistemaOperatiu;
    }
}

//Creo un reloj para ver si funciona el constructor y el método print
rellotge1 = new Rellotge('Casio', '3000Super', 15);
console.log(rellotge1.print());

//Creo un catálogo y añado un reloj estándar
cataleg = new Cataleg();
cataleg.addRellotge(rellotge1);
console.log(cataleg.print());

//Creo y añado un reloj de cada tipo para probar los contadores
mecanic1 = new Mecanic('Nimbus', 'Black100', 53, 'Bona');
cataleg.addRellotge(mecanic1);
console.log(cataleg.print());

electronic1 = new Electronic('Caplypso', 'Electro', 26, 'Esportiu', 'A+');
cataleg.addRellotge(electronic1);
console.log(cataleg.print());

smartwatch1 = new Smartwatch('Xiaomi', 'MiWatch', 94, 'Esportiu', 'Android');
cataleg.addRellotge(smartwatch1);
console.log(cataleg.print());

//Creo 1 reloj mecánico, 1 electrónico y 3 smartwatchs
mecanic2 = new Mecanic('Rolex', 'Millioner', 78, 'Dolenta');
electronic2 = new Electronic('Yanis', 'Juanito', 13, 'Streamer', 'Rodona');
smartwatch2 = new Smartwatch('Xiaomi', 'Mi2', 257, 'Formal', 'Android');
smartwatch3 = new Smartwatch('Samsung', 'Galaxy3', 31, 'Esportiu', 'Android');
smartwatch4 = new Smartwatch('iPhone', 'iWatch', 24, 'Laboral', 'iOS');

//Añado todos los relojes al catálogo y compruebo los contadores
cataleg.addRellotge(mecanic2);
cataleg.addRellotge(electronic2);
cataleg.addRellotge(smartwatch2);
cataleg.addRellotge(smartwatch3);
cataleg.addRellotge(smartwatch4);
console.log(cataleg.print());